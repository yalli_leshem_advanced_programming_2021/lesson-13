#include "ClientHelper.h"

std::string ClientHelper::getChatLog(const std::string firstUser, const std::string secondUser)
{
	std::string message;
	std::string allMesssges;

	// open file for reading
	std::fstream ChatFile;
	if (firstUser.size() != 0 && secondUser.size() != 0)
	{
		ChatFile.open(getChatFileName(firstUser, secondUser), std::fstream::in);

		if (ChatFile.is_open()) // make sure the file opened properly
		{
			while (std::getline(ChatFile, message))
			{
				allMesssges.append(message);
			}

			ChatFile.close();
		}
		return allMesssges;
	}
	
	return "";
}

void ClientHelper::updateAChatLog(const messageFormat message)
{
	// open file for writing
	std::ofstream chatFile(getChatFileName(message.receiverUsername, message.senderUsername));

	chatFile << "&MAGSH_MESSAGE&&Author&" << message.senderUsername << "&DATA&" << message.message << std::endl;

	chatFile.close();
}

std::string ClientHelper::joinAllUsernames(const std::set<std::string> userList)
{
	std::string joinedNames;

	// joins all the names with & as separator
	for (std::string name : userList)
	{
		joinedNames.append(name);
		joinedNames.append("&");
	}

	// always ends with an extra &, so remove it
	if (!joinedNames.empty()) 
		joinedNames.pop_back();

	return joinedNames;
}

std::string ClientHelper::getChatFileName(const std::string firstUser, const std::string secondUser)
{
	if (firstUser.size() != 0 && secondUser.size() != 0)
	{
		if (firstUser < secondUser) // if smaller then the first username comes first in the file name
		{
			return firstUser + '&' + secondUser + ".txt";
		}
		else
			return secondUser + '&' + firstUser + ".txt";
	}
	return "";
}
