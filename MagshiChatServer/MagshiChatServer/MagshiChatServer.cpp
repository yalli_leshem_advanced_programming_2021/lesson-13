#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server::_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		Server::serve(8826);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}