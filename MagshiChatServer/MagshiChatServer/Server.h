#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <set>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include "Helper.h"
#include "ClientHelper.h"

/// <summary>
/// enum of the type codes in the magshichat protocol
/// </summary>
enum class typeCodes
{
	LOGIN_CODE = 200,
	SERVER_UPDATE_CODE = 101,
	CLIENT_UPDATE_CODE = 204
};

#define MOST_PROTOCOL_FIELDS_LEN 5
#define USERNAME_FIELD_LEN 2

class Server
{
public:
	static void serve(int port);
	static SOCKET _serverSocket;

private:
	static std::set< std::string > _connectedUsers;
	static std::vector< messageFormat > _messages;

	static std::mutex _msgMtx;
	static std::mutex _usersMtx;
	static std::condition_variable _newMsgAlert;

	static void accept();

	static void clientHandler(SOCKET clientSocket, const std::string username);
	
	/// <summary>
	/// this will check if the first message is a valid login message (200[userlen][username]), 
	/// if it is then add the user and open a thread for him
	/// </summary>
	/// <param name="clientSocket">the socket with the client</param>
	static void tryToAddUser(SOCKET clientSocket);

	/// <summary>
	/// (function is only used when the type code is 204 and the code is already out of the stream not in the socket buffer) 
	/// sends the client a server Update Message, then if the client sent a new message (new msg = len isn't 0)
	/// then the function sends the new message to queue
	/// </summary>
	/// <param name="client_socket">the socket stream</param>
	/// <param name="username">the clients name</param>
	static void handleClientUpdate(SOCKET client_socket, const std::string username);

	/// <summary>
	/// function takes the messages out of the queue and into the chat files
	/// </summary>
	static void updateAllChatLogs();
};
