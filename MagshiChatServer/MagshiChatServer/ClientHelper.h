#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <fstream>
#include <string>
#include <iostream>
#include <set>

/// <summary>
/// struct for all messages, each new message received will need to fit this format before going in the messages queue 
/// </summary>
struct messageFormat
{
	std::string senderUsername;
	std::string receiverUsername;

	std::string message;
};

class ClientHelper
{
public:
	/// <summary>
	/// get the chat content of the two users
	/// </summary>
	/// <param name="firstUser">the first user name (user order doesn't matter)</param>
	/// <param name="secondUser">the second user name (user order doesn't matter)</param>
	/// <returns>the content of the file that has all the messages the two users exchanged</returns>
	static std::string getChatLog(const std::string firstUser, const std::string secondUser);

	/// <summary>
	/// add the new message to the file that contains the chat history between the two users
	/// *assumes the message is properly formated and the users are valid*
	/// </summary>
	/// <param name="message">the new message properly formated</param>
	static void updateAChatLog(const messageFormat message);

	/// <summary>
	/// joins all strings in the list to one string with '&amp;' dividing between each string  
	/// </summary>
	/// <param name="userList">the list of all the usernames</param>
	/// <returns>all the strings joined into one string (for example if there are 2 strings 'bob' and 'job' the joined string will be 'bob&amp;job'</returns>
	static std::string joinAllUsernames(const std::set< std::string > userList);

private:
	/// <summary>
	/// function returns the name of the file that contains the chat between the two users
	/// </summary>
	/// <param name="firstUser"></param>
	/// <param name="secondUser"></param>
	/// <returns></returns>
	static std::string getChatFileName(const std::string firstUser, const std::string secondUser);
};

