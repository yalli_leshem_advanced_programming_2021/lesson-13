#include "Server.h"
#include <exception>
#include <string>

std::set <std::string> Server::_connectedUsers{};
std::vector< messageFormat > Server::_messages{};

std::mutex Server::_msgMtx{};
std::mutex Server::_usersMtx{};
std::condition_variable Server::_newMsgAlert{};

SOCKET Server::_serverSocket{};

void Server::serve(int port)
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	// create a thread to move messages from the queue to there file
	std::thread messagesQueueHandlerThread([&] {updateAllChatLogs(); });

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		try
		{
			accept();
		}
		catch(...){}
	}
}

void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	Server::tryToAddUser(client_socket);
}

void Server::clientHandler(SOCKET clientSocket, const std::string username)
{
	try
	{ 
		while (true)
		{
			if (Helper::getMessageTypeCode(clientSocket) == int(typeCodes::CLIENT_UPDATE_CODE))
				Server::handleClientUpdate(clientSocket, username);
		}

	}
	catch (...)
	{
		closesocket(clientSocket);
		Server::_connectedUsers.erase(username);
		return;
	}
	closesocket(clientSocket);
	Server::_connectedUsers.erase(username);
}

void Server::tryToAddUser(SOCKET clientSocket)
{
	// check that the first message received by the client is really a login message
	if (unsigned int(typeCodes::LOGIN_CODE) == Helper::getMessageTypeCode(clientSocket))
	{
		// get the username
		int usernameLen = Helper::getIntPartFromSocket(clientSocket, USERNAME_FIELD_LEN);
		std::string username = Helper::getStringPartFromSocket(clientSocket, usernameLen);

		// check if user already exists
		if (Server::_connectedUsers.count(username) == 1) // if user already connected
		{
			closesocket(clientSocket);
			return;
		}
		else // if user isn't already connected
		{
			
			{ // insert a new user
				std::lock_guard <std::mutex> lockUsers(Server::_usersMtx);

				Server::_connectedUsers.insert(username); // add user to the list
			}
			Helper::send_update_message_to_client(clientSocket, " ", " ", ClientHelper::joinAllUsernames(Server::_connectedUsers)); // send first update to user

			// make a client thread
			std::thread clientThread(&Server::clientHandler, clientSocket, username);
			clientThread.detach();
		}
	}
	else
		closesocket(clientSocket);
}

void Server::handleClientUpdate(SOCKET client_socket, const std::string username)
{
	std::string secondUser = "";
	std::string chatLog = "";

	// get the second user name
	int secondUserLen = Helper::getIntPartFromSocket(client_socket, USERNAME_FIELD_LEN);	
	secondUser = Helper::getStringPartFromSocket(client_socket, secondUserLen);

	// get all the messages exchanged between the two users
	chatLog = ClientHelper::getChatLog(username, secondUser);
	
	// get all usernames joined into one string
	std::string allUsers = ClientHelper::joinAllUsernames(Server::_connectedUsers);

	// add the new message (only if there is one) to the queue 
	int newMessageLen = Helper::getIntPartFromSocket(client_socket, MOST_PROTOCOL_FIELDS_LEN);
	if (newMessageLen != 0) // if 0 is returned there is no new message
	{
		// format message
		messageFormat newMessage;
		newMessage.message = Helper::getStringPartFromSocket(client_socket, newMessageLen);

		newMessage.receiverUsername = secondUser;
		newMessage.senderUsername = username;

		// lock access to the _messages queue 
		std::unique_lock<std::mutex> msgLocker(Server::_msgMtx);
		Server::_messages.push_back(newMessage); // add the message to the queue

		// notify the server that a new message was just pushed into the queue
		Server::_newMsgAlert.notify_one();
		msgLocker.unlock();
	}
	Helper::send_update_message_to_client(client_socket, chatLog, secondUser, allUsers);
}

void Server::updateAllChatLogs()
{
	while (true) // run until server is closed
	{
		// wait for a new message to be pushed in the queue
		std::unique_lock<std::mutex> msgLocker(Server::_msgMtx);
		Server::_newMsgAlert.wait(msgLocker);

		while (!Server::_messages.empty()) // pop all messages in line
		{
			messageFormat msg = Server::_messages.front(); // save the first message in line
			Server::_messages.pop_back(); // remove the first message in line

			ClientHelper::updateAChatLog(msg);
		}
		msgLocker.unlock(); // unlock queue access
	}
}
